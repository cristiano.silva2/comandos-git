# Comandos Git

Comandos Git Lab

Command line instructions
You can also upload existing files from your computer using the instructions below.


Git global setup
``````
git config --global user.name "Cristiano Feliciano da Silva"
git config --global user.email "cristiano.silva@mandic.net.br"
``````

Create a new repository
``````
git clone git@gitlab.com:cristiano.silva2/fsrdgfdsg.git
cd fsrdgfdsg
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master
``````

Push an existing folder
``````
cd existing_folder
git init
git remote add origin git@gitlab.com:cristiano.silva2/fsrdgfdsg.git
git add .
git commit -m "Initial commit"
git push -u origin master
``````

Push an existing Git repository
``````
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:cristiano.silva2/fsrdgfdsg.git
git push -u origin --all
git push -u origin --tags
``````

Comando para imputar imagem README

`![](Fluxograma.png)`
